const {server, app} = require('./index.js')
const supertest = require('supertest')



afterAll(done => {
    server.close(done);
})

test("GET /employee", async () => {
  
  await supertest(app)
    .get("/employee")
    .expect(200)
    .then((response) => {
      
      expect(Array.isArray(response.body)).toBeTruthy()
      expect(response.body.length).toBeGreaterThanOrEqual(1)

      // Check the response data
      //expect(response.body[0].id).toEqual(expect.anything()) 
      
      expect(response.body[0]).toHaveProperty('id')
      expect(response.body[0]).toHaveProperty('first_name')
      expect(response.body[0]).toHaveProperty('last_name')
      expect(response.body[0]).toHaveProperty('phone_number')
      expect(response.body[0]).toHaveProperty('service')
      
    })
})
