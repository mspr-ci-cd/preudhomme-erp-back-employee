# Preudhomme ERP Back Employee
> cbarange | 07th March 2021
---

## Build Setup

```bash
# install dependencies
yarn install

# run for development
yarn dev

# run for production
yarn start

# run test
yarn test
```
