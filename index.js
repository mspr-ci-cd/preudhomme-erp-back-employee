/** 
 * @file API Back - Employees
 * 
 * @version 1.0
 * @author Jules Peguet
 * @copyright 2021
 * 
 */

require('dotenv').config()

const data = require('./data.json')

const express = require('express')
const morgan = require('morgan')
const cors = require('cors')
const swagger_ui = require('swagger-ui-express')
const swagger_document = require('./swagger.json')

const app = express()
app.enable('trust proxy')
app.use(express.json())
app.use(morgan('common'))
app.use(cors('*'))
app.use('/docs', swagger_ui.serve, swagger_ui.setup(swagger_document))

/**
 * get all the employees in the database
 * @method getEmployee
 * @returns {json} - The list of Employees
 */
const getEmployee = () => {
  return data;
}

app.get('/employee', async (req, res) => {
  res.json(getEmployee())
})

const port = process.env.PORT || 5225
/**
 * Start the express server
 */
const server = app.listen(port, () => console.log(`Listening at http://localhost:${port}`))

// Export module for test
module.exports = {app:app,server:server}